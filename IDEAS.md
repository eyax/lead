This is a place to post ideas about the language

- A native stdlib-free way of handling endianness
- Simplified Recursive Templates
- Modules
- Standardized implicit constructors
- Static arrays, Semi-static arrays and Dynamic arrays [ARRAYS.md]
- Compile Time Attributes
- Epsilon operator (operator∈()), easy use for Foo ∈ Bar (Foo is an element of Bar)
- Promises (to avoid runtime errors)