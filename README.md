# The Lead Programming Language
**A language built to be easy and safe for everyone, with a C-style syntax.**

*Lead* is a language inspired by modern languages such as [*Rust*](https://www.rust-lang.org) or [*D*](https://dlang.org/), and by classic languages such as *C++* or *C#*.

It's main feature is the [memory ownership system borrowed from Rust](https://doc.rust-lang.org/nomicon/ownership.html). It's a safe manual memory management system that makes *memory leaks* or *references to deleted memory* **impossible**. It has no overhead like a garbage collector would have as it is a **compile-time check** . And it is safe and implemented in the language, unlike C++'s *unique_ptr* that are part of the Standard Library and not mandatory.

Example :

```cpp
//C++ code

class Bar
{
	void method(){}
};

void foo()
{
	Bar* bar = new Bar();
	Bar& ref = *bar; //Reference to the Bar object

	delete bar; //The memory is deleted

	ref.method(); //Runtime error!
}
```

This C++ code compiles just fine, even if it's obviously not safe. The error is caught at runtime, with is extremely dangerous.

This is an obvious example, but errors like this can be hidden, and will never be spotted until someone triggers the exact scenario to make it happen.

Let's try to reproduce this error in Lead :

```cpp
//Lead code

class Bar
{
	void method(){}
};

void foo()
{
	Bar bar;
	Bar ref = &bar; //Reference to the Bar object, ref is 'borrowing' the access to the memory

	delete bar; //Compile time error : reference 'ref' has a longer lifetime that owner 'bar'

	ref.method();
}
```

Here the compiler doesn't even let us delete the object while a reference still exists. The reference has to be explicitly deleted by the coder or will be deleted at the end of the scope it was created in.

Example :

```cpp
//Lead code

class Bar
{
	void method(){}
};

void foo()
{
	Bar bar;
	Bar ref = &bar; //Reference to the Bar object, ref is 'borrowing' the access to the memory

	delete ref; //Now there is no reference pointing towards 'bar'

	ref.method(); //Compile time error : Use of deleted reference 'ref'
}
```
