# Arrays in Lead

Arrays are any container that stores data **contiguously**. There are multiple types of Arrays :

 - Static Arrays : Their size is known at compile-time.
 - Dynamic Arrays (also known as Vectors) : Their size is defined at runtime, and can change.

## Static Arrays

    int[5] staticArray;

Static Arrays are special, **as their size is part of their type**. Here, *staticArray* is a static array of size 5.
You can access the data inside staticArray by using *operator[]*.

    staticArray[2] = 5; //3rd element is 5
Because the size is known at compile-time, the compiler can prevent Segmentation Faults

    staticArray[8] = 2; //compile-time error : staticArray has a compile-time size of 5
	staticArray.size(); //compile-time constant
   
   To access the inside of a static array using a variable (set at runtime), the variable needs to be **ranged using promises** (promises are not yet specified in the language).


## Dynamic Arrays (vectors)

    int[] dynamicArray;

Vectors can be resized, reconstructed, pushed back, etc..

    dynamicArray.push_back(9);
	dynamicArray.size(); //runtime value
